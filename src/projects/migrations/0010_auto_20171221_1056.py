# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-21 09:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0009_auto_20171221_0944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='keyword',
            name='label',
            field=models.CharField(default='', max_length=5000, verbose_name='Label'),
        ),
        migrations.AlterField(
            model_name='keyword',
            name='value',
            field=models.SlugField(default='', max_length=5000),
        ),
        migrations.AlterField(
            model_name='project',
            name='abstract',
            field=models.TextField(blank=True, null=True, verbose_name='Abstract'),
        ),
        migrations.AlterField(
            model_name='project',
            name='contact',
            field=models.CharField(blank=True, max_length=5000, null=True, verbose_name='Contact'),
        ),
        migrations.AlterField(
            model_name='project',
            name='date',
            field=models.CharField(blank=True, max_length=5000, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='project',
            name='economic',
            field=models.CharField(blank=True, max_length=5000, null=True, verbose_name='Economic'),
        ),
        migrations.AlterField(
            model_name='project',
            name='keywords',
            field=models.ManyToManyField(blank=True, to='projects.Keyword', verbose_name='Keywords'),
        ),
        migrations.AlterField(
            model_name='project',
            name='partner',
            field=models.TextField(blank=True, null=True, verbose_name='Partner'),
        ),
        migrations.AlterField(
            model_name='project',
            name='project_holder',
            field=models.CharField(blank=True, max_length=5000, null=True, verbose_name='Project Holder'),
        ),
        migrations.AlterField(
            model_name='project',
            name='title',
            field=models.CharField(default='', max_length=5000, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='project',
            name='video',
            field=models.TextField(blank=True, null=True, verbose_name='Video URL'),
        ),
    ]
