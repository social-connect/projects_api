# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-15 09:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0003_auto_20171208_1046'),
    ]

    operations = [
        migrations.RenameField(
            model_name='keyword',
            old_name='name',
            new_name='label',
        ),
        migrations.AddField(
            model_name='keyword',
            name='value',
            field=models.SlugField(default=''),
        ),
    ]
