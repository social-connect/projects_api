from django.db.models import Count
from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic import ListView
from projects.models.project import Project
from projects.models.contributor import Contributor


class HomePageView(TemplateView):
    """Homepage view"""

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['projects_count'] = Project.objects.count()
        context['contributors_count'] = Contributor.objects.count()
        return context


class ContributorListView(ListView):
    """Contributor list view

See documentation on https://docs.djangoproject.com/fr/1.11/ref/class-based-views/generic-display/#listview)
"""
    model = Contributor
    queryset = Contributor.objects.order_by('name').annotate(projects_count=Count('project'))
    context_object_name = 'publisher_list'
    ordering = 'name'
