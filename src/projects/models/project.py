from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _


class Project(models.Model):
    """
    Project model
    """
    title = models.CharField(verbose_name=_("Title"), max_length=5000, default="")
    contributor = models.ForeignKey(
        'Contributor', verbose_name=_('Contributor'), blank=True, null=True
    )
    abstract = models.TextField(verbose_name=_("Abstract"), blank=True, null=True)
    keywords = models.ManyToManyField(
        'Keyword', verbose_name=_("Keywords"), related_name='used_by', blank=True
    )
    link = models.URLField(verbose_name=_("Project URL"), blank=True, null=True)
    img = models.URLField(verbose_name=_("Image URL"), blank=True, null=True)
    date = models.CharField(verbose_name=_("Date"), max_length=5000, blank=True, null=True)
    area = models.CharField(verbose_name=_("Area"), max_length=1000, blank=True, null=True)
    latitude = models.FloatField(verbose_name=_("Latitude"), blank=True, null=True)
    longitude = models.FloatField(verbose_name=_("Longitude"), blank=True, null=True)
    contact = models.CharField(verbose_name=_("Contact"), max_length=5000, blank=True, null=True)
    video = models.TextField(verbose_name=_("Video URL"), blank=True, null=True)
    state = models.CharField(verbose_name=_("State"), max_length=1000, blank=True, null=True)
    project_holder = models.CharField(
        verbose_name=_("Project Holder"), max_length=5000, blank=True, null=True
    )
    partner = models.TextField(verbose_name=_("Partner"), blank=True, null=True)
    economic = models.CharField(verbose_name=_("Economic"), max_length=5000, blank=True, null=True)

    class Meta:
        verbose_name = _("Project")
        verbose_name_plural = _("Projects")

    def __str__(self):
        return self.title
