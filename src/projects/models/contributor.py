from django.db import models
from django.utils.translation import ugettext_lazy as _

from projects.models.project import Project

class Contributor(models.Model):
    """
    Project model
    """
    name = models.CharField(verbose_name=_("Contributor name"), max_length=1000, default="")
    spider_name = models.CharField(verbose_name=_("Spider name"), max_length=1000, default="")
    link = models.URLField(verbose_name=_("Contributor URL"), blank=True, null=True)

    class Meta:
        verbose_name = _("Contributor")
        verbose_name_plural = _("Contributor")

    def __str__(self):
        return self.name
