from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _


class Keyword(models.Model):
    """
    Key word model : to store key words in database
    """
    label = models.CharField(verbose_name=_("Label"), max_length=5000, default="")
    value = models.SlugField(max_length=5000, default="", unique=True)


    class Meta:
        verbose_name = _("Keyword")
        verbose_name_plural = _("Keywords")

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        if not self.id:
            self.value = slugify(self.label)

        super(Keyword, self).save(*args, **kwargs)
