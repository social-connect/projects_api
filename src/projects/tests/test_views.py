from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client

from projects.models import project, keyword, contributor
from projects import factory as proj_factory


class ViewsProjectsTest(TestCase):
    """
    TestCase for projects app views
    """

    @classmethod
    def setUpTestData(cls):
        """Setup client test data"""
        cls.client = Client()
        cls.contributors = proj_factory.ContributorFactory.create_batch(3)
        cls.projects = proj_factory.ProjectFactory.create_batch(15, fetch_contributors = True)

    def test_get_home(self):
        """Test get homepage"""
        url = reverse("home")
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)

    def test_count_elements_in_homepage(self):
        """Test count elements in homepage context"""
        url = reverse("home")
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(len(self.projects), response.context['projects_count'])
        self.assertEqual(len(self.contributors), response.context['contributors_count'])
