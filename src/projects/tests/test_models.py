from django.test import TestCase
from django.test.client import Client
from django.utils.text import slugify

from projects.models import project, keyword, contributor
from projects import factory as proj_factory


class ModelsProjectsTest(TestCase):
    """
    TestCase for projects app models
    """
    def setUp(self):
        """Create contributors and projects"""
        self.one_contributor = proj_factory.ContributorFactory.create()
        self.one_project = proj_factory.ProjectFactory.create(contributor = self.one_contributor)
        self.one_keyword = proj_factory.KeywordFactory.create()

    def test_project_creation(self):
        """Test project creation"""
        self.assertTrue(isinstance(self.one_project, project.Project))
        self.assertEqual(self.one_project.__str__(), self.one_project.title)
        self.assertTrue(isinstance(self.one_project.contributor, contributor.Contributor))

    def test_keyword_creation(self):
        """Test keyword creation"""
        self.assertTrue(isinstance(self.one_keyword, keyword.Keyword))
        self.assertEqual(self.one_keyword.__str__(), self.one_keyword.label)
        self.assertEqual(self.one_keyword.value, slugify(self.one_keyword.label))

    def test_contributor_creation(self):
        """Test contributor creation"""
        self.assertTrue(isinstance(self.one_contributor, contributor.Contributor))
        self.assertEqual(self.one_contributor.__str__(), self.one_contributor.name)
