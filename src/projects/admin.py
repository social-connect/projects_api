from django.contrib import admin
from django.db.models import Q
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from .models.keyword import Keyword
from .models.project import Project
from .models.contributor import Contributor

items_per_page = 20


class YesNoFilter(admin.SimpleListFilter):
    """Yes / No basic filter"""

    def lookups(self, request, model_admin):
        return (
            ('yes', _('Yes')),
            ('no', _('No')),
        )


class ImageListFilter(YesNoFilter):
    """Filter for projects with or without image"""

    title = _('has image')
    parameter_name = 'has_image'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(img__isnull=False).exclude(img='')
        if self.value() == 'no':
            return queryset.filter(Q(img__isnull=True) | Q(img__exact=''))


class VideoListFilter(YesNoFilter):
    """Filter for projects with or without video"""

    title = _('has video')
    parameter_name = 'has_video'

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(video__isnull=False).exclude(video='').exclude(video='[]')
        if self.value() == 'no':
            return queryset.filter(
                Q(video__isnull=True) | Q(video__exact='') | Q(video__exact='[]')
            )


class UnusedKeywordsFilter(YesNoFilter):
    """Filter keywords used or not by projects"""

    title = _('used by projects')
    parameter_name = 'is_used'

    def queryset(self, request, queryset):
        projects_list = Project.objects.all().values_list('pk', flat=True)
        if self.value() == 'yes':
            return queryset.filter(used_by__in=list(projects_list)).distinct()
        if self.value() == 'no':
            return queryset.exclude(used_by__in=list(projects_list))


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    """Admin class for Project model"""

    list_per_page = items_per_page
    list_display = ('title', 'contributor', 'show_url', 'date', )
    ordering = ('title',)
    search_fields = ('title', 'abstract', 'keywords__label',)
    list_filter = ('contributor__name', ImageListFilter, VideoListFilter,)
    filter_horizontal = ('keywords',)

    def show_url(self, instance):
        return format_html('<a href="{}" target="_blank">View on site</a>', instance.link)


@admin.register(Keyword)
class KeywordAdmin(admin.ModelAdmin):
    """Admin class for Keyword model"""

    list_per_page = items_per_page
    list_display = ('label',)
    ordering = ('label',)
    search_fields = ('label',)
    list_filter = (UnusedKeywordsFilter,)
    prepopulated_fields = {"value": ("label",)}


@admin.register(Contributor)
class ContributorAdmin(admin.ModelAdmin):
    """Admin class for Contributor model"""

    list_per_page = items_per_page
    list_display = ('name', )
    ordering = ('name',)
    search_fields = ('name',)
