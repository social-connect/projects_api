from django.contrib.auth import models as auth_models
import factory
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyText

from projects.models import project, keyword, contributor


class UserFactory(DjangoModelFactory):
    class Meta:
        model = auth_models.User

    username = factory.Sequence(lambda n: "user_%d" % n)


class KeywordFactory(DjangoModelFactory):
    """Factory class for projects"""
    value = FuzzyText()

    class Meta:
        model = keyword.Keyword


class ContributorFactory(DjangoModelFactory):
    """Factory class for projects"""
    name = FuzzyText(length=15)

    @factory.lazy_attribute
    def spider_name(self):
        clean_name = self.name.replace(' ', '').lower()
        return clean_name

    class Meta:
        model = contributor.Contributor


class ProjectFactory(DjangoModelFactory):
    """Factory class for projects"""
    title = FuzzyText()
    contributor = factory.SubFactory(ContributorFactory)
    abstract = FuzzyText(length=200)

    class Meta:
        model = project.Project

    class Params:
        fetch_contributors = factory.Trait(
            contributor=factory.Iterator(contributor.Contributor.objects.order_by('?')),
        )
