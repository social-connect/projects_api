from rest_framework import serializers
from django.db import models
from projects.models.contributor import Contributor


class ContributorSerializer(serializers.ModelSerializer):
    """
    Contributor serialier
    """
    class Meta:
        model = Contributor
        fields = '__all__'
