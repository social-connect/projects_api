from rest_framework import serializers
from django.db import models

from projects.models.keyword import Keyword
from projects.models.project import Project
from projects.models.contributor import Contributor
from api.serializers.keyword import KeywordSerializer
from api.serializers.contributor import ContributorSerializer


class ProjectSerializer(serializers.ModelSerializer):
    """
    Project serializer
    """
    keywords = serializers.SerializerMethodField()
    coordinates = serializers.SerializerMethodField()
    contributor = ContributorSerializer(read_only=True)
    contributor_id = serializers.PrimaryKeyRelatedField(queryset=Contributor.objects.all(), write_only=True)
    keywords_id = serializers.PrimaryKeyRelatedField (queryset=Keyword.objects.all(), write_only=True, many=True)

    class Meta:
        model = Project
        fields = (
            'id', 'title', 'abstract', 'link', 'date', 'keywords', 'keywords_id',
            'area', 'contributor_id', 'contributor', 'longitude', 'latitude',
            'contact', 'project_holder', 'img', 'state', 'economic', 'video', 'partner', 'coordinates'
        )

    def get_keywords(self, obj):
        """Get keywords label"""
        return [keyword.label for keyword in obj.keywords.all()]

    def get_coordinates(self, obj):
        """Get coordinates"""
        return [obj.longitude, obj.latitude]

    def create(self, validated_data):
        # Override default `.create()` method in order to properly add `sport` and `category` into the model
        contributor_id = validated_data.pop('contributor_id')
        keywords_id = validated_data.pop('keywords_id')
        project = Project.objects.create(contributor=contributor_id,**validated_data)
        for keyword_id in keywords_id:
            project.keywords.add(keyword_id)
        return project
