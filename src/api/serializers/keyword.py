from rest_framework import serializers
from django.db import models
from projects.models.keyword import Keyword


class KeywordSerializer(serializers.ModelSerializer):
    """
    KeyWord serialier
    """
    class Meta:
        model = Keyword
        fields = '__all__'
