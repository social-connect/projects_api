from django.db.models import Q
from rest_framework import filters, permissions, viewsets
from rest_framework.pagination import PageNumberPagination
from django_filters import rest_framework as filters

from projects.models.project import Project
from projects.models.keyword import Keyword
from projects.models.contributor import Contributor
from .serializers.project import ProjectSerializer
from .serializers.keyword import KeywordSerializer
from .serializers.contributor import ContributorSerializer


class LargeResultsSetPagination(PageNumberPagination):
    """Pagination class"""
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 10000



class ProjectFilter(filters.FilterSet):
    """Project filter
Add search filter to search in both title and abstract
    """
    search = filters.CharFilter(method='search_filter')

    class Meta:
        model = Project
        fields = {
            'search': ['exact'],
            'title': ['exact', 'icontains', ],
            'link': ['exact',],
            'abstract': ['icontains', ],
            'keywords': ['exact', ],
            'keywords__value': ['in', ],
            'contributor': ['exact', ],
            'contributor__spider_name': ['exact', ]
        }

    def search_filter(self, queryset, name, value):
        return queryset.filter(Q(title__icontains=value) | Q(abstract__icontains=value))


class ProjectViewsSet(viewsets.ModelViewSet):
    """
    API endpoint for projects to be viewed or edited.

    list:
    List projects, ordered by title

    retrieve:
    Return a project instance
    """
    queryset = Project.objects.get_queryset().order_by('id')
    serializer_class = ProjectSerializer
    pagination_class = LargeResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectFilter


class KeywordViewSet(viewsets.ModelViewSet):
    """
    API endpoint for keywords to be viewed or edited.

    list:
    List keywords, ordered by title

    retrieve:
    Return a keyword instance
    """
    queryset = Keyword.objects.get_queryset().order_by('value')
    serializer_class = KeywordSerializer
    pagination_class = LargeResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = {
        'label': ['exact', 'iexact', 'icontains', ],
        'value': ['exact']
    }

class ContributorViewSet(viewsets.ModelViewSet):
    """
    API endpoint for contributor to be viewed or edited.

    list:
    List contributor, ordered by title

    retrieve:
    Return a contributor instance
    """
    queryset = Contributor.objects.get_queryset().order_by('spider_name')
    serializer_class = ContributorSerializer
    pagination_class = LargeResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = {
        'name': ['exact', 'icontains', ],
        'spider_name': ['exact', 'icontains', ]
    }
