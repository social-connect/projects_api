import json

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils.text import slugify
from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from projects.models import project, keyword, contributor
from projects import factory as proj_factory


class BaseApiTest(APITestCase):
    """
    TestCase for API views
    """
    @classmethod
    def setUpTestData(cls):
        cls.projects = proj_factory.ProjectFactory.create_batch(15)
        cls.allowed_user = proj_factory.UserFactory.create(username="allowed_user")

        cls.project_url = reverse('api:project-list')
        cls.keyword_url = reverse('api:keyword-list')
        cls.keyword_json = {'label': 'My keyword'}
        cls.contributor_url = reverse('api:contributor-list')

    def get_token(self, user):
        return Token.objects.get(user=user)


class ProjectsAPITestCase(BaseApiTest):
    """
    TestCase for projects API
    """
    def test_get_projects_anonymous(self):
        """Test project list API anonymous"""
        self.client.logout()
        response = self.client.get(self.project_url)
        self.assertEqual(response.status_code, 200)


class KeywordsAPITestCase(BaseApiTest):
    """
    TestCase for keywords API
    """
    def test_get_keywords_anonymous(self):
        """Test keywords list API anonymous"""
        self.client.logout()
        response = self.client.get(self.keyword_url)
        self.assertEqual(response.status_code, 200)

    def test_post_keywords_anonymous(self):
        """Test keywords post API anonymous"""
        self.client.logout()
        response = self.client.post(self.keyword_url, {'label': 'My keyword'})
        self.assertEqual(response.status_code, 401)

    def test_post_keywords_allowed_user(self):
        """Test keywords post API with allowed user token"""
        allowed_token = self.get_token(self.allowed_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + allowed_token.key)
        response = self.client.post(self.keyword_url, data=self.keyword_json)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['value'], slugify(self.keyword_json['label']))


class ContributorsAPITestCase(BaseApiTest):
    """
    TestCase for contributors API
    """
    def test_get_contributors_anonymous(self):
        """Test contributors list API anonymous"""
        self.client.logout()
        response = self.client.get(self.contributor_url)
        self.assertEqual(response.status_code, 200)

    def test_post_contributors_anonymous(self):
        """Test contributors post API anonymous"""
        self.client.logout()
        response = self.client.post(self.contributor_url, {'name': 'Super Contrib'})
        self.assertEqual(response.status_code, 401)

    def test_post_contributors_allowed_user(self):
        """Test contributors post API with allowed user token"""
        allowed_token = self.get_token(self.allowed_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + allowed_token.key)
        response = self.client.post(
            self.contributor_url,
            data={'name': 'Super Contrib'}
        )
        self.assertEqual(response.status_code, 201)
