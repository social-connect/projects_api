# -*- coding: utf-8 -*-
import logging

from django.utils import six

from .base import *

try:
    from .local.base_pre import *
except ImportError:
    pass

try:
    from .local.dev_pre import *
except ImportError:
    pass


SECRET_KEY = 'dev-dev-dev-dev-dev-dev-dev'

ALLOWED_HOSTS = ['*']
DEBUG = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS += (
    'debug_toolbar',
    'django_extensions',
    'debug_panel',
)

MIDDLEWARE += (
    'debug_panel.middleware.DebugPanelMiddleware',
)

CORS_ORIGIN_ALLOW_ALL = True

INTERNAL_IPS = ('127.0.0.1',)  # Used by app debug_toolbar

try:
    from .local.base_post import *
except ImportError:
    pass

try:
    from .local.dev_post import *
except ImportError:
    pass
