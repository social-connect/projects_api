# -*- coding: utf-8 -*-
from .base import *

try:
    from .local.base_pre import *
except ImportError:
    pass

try:
    from .local.staging_pre import *
except ImportError:
    pass

DEBUG = True

# Change 'default' database configuration with $DATABASE_URL.
DATABASES['default'].update(dj_database_url.config(conn_max_age=500))

ALLOWED_HOSTS = ['social-connect-cget.herokuapp.com']
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

##### SECURITY #####

SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'
CSRF_COOKIE_HTTPONLY = True

# Suppose we are using HTTPS
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = True # Just in case, should be done by webserver instead

CORS_ORIGIN_ALLOW_ALL = True

try:
    from .local.base_post import *
except ImportError:
    pass

try:
    from .local.staging_post import *
except ImportError:
    pass
