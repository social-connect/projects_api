"""CGET URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from api import views as api_views
from projects.views import HomePageView, ContributorListView

router = routers.DefaultRouter()
router.register(r'projects', api_views.ProjectViewsSet)
router.register(r'keywords', api_views.KeywordViewSet)
router.register(r'contributors', api_views.ContributorViewSet)

schema_view = get_swagger_view(title='Social projects API')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/docs/$', schema_view, name='api-docs'),
    url(r'^api/', include(router.urls, namespace='api')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^contributeurs/$', ContributorListView.as_view(), name='contributors-list'),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^$', HomePageView.as_view(), name='home'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
