#!/usr/bin/env python
import os
import sys

f = __file__
if os.path.islink(f):
    f = os.readlink(__file__)
f = os.path.abspath(f)
d = os.path.dirname(f)
src_dir = os.path.join(d, '.')

envs = ['prod', 'test', 'dev']
if len(sys.argv) > 1 and sys.argv[1] == 'test':
    envs = ['test']

mod_settings = 'settings'
for i in os.listdir(src_dir):
    ip = os.path.join(src_dir, i)
    ips = os.path.join(ip, 'settings')
    if os.path.exists(ips + '.py'):
        mod_settings = i + '.settings'
    else:
        for j in envs:
            psettings = i + '.settings.' + j
            if os.path.exists(os.path.join(ips, j + '.py')):
                mod_settings = psettings

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", mod_settings)
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    execute_from_command_line(sys.argv)
