# Banque de l'innovation territoriale : backend

## Installation

Installer le projet et ses sous-modules

    git clone --recursive git@gitlab.makina-corpus.net:cget/cget_django.git

Installer les dépendances python

    cd cget_django
    pipenv install

Installer le projet django

    pipenv shell
    cd src
    python manage.py migrate
    python manage.py runserver

## Déployer sur Heroku

Déployer

    git push heroku master

Lancer une commande avec heroku

    heroku run <commande>

Lancer la migration de la base de données

    heroku run python src/manage.py migrate

Créer un super utilisateur

    heroku run python src/manage.py createsuperuser